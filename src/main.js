const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


let html = '';
data.filter(function({name}) {
    return name.split('i').length==3;
}).forEach(element => {
    const name = element.name;
    const url = element.image;
    const small = element.price_small;
    const high = element.price_large;
    const lienimage = `<a href="${url}">
        <img src="${url}"/>
        <section>
            <h4>${name}</h4>
            <ul>
                <li>Prix petit format : ${small.toFixed(2)} €</li>
                <li>Prix grand format : ${high.toFixed(2)} €</li>
            </ul>
        </section>
    </a>`;
    html = html + `<article class="pizzaThumbnail">
        ${lienimage}
    </article>`;
    document.querySelector('.pageContent').innerHTML = html;
    console.log(html);
});